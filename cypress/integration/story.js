const june16th = new Date(Date.UTC(2020, 5, 16)).getTime()
const june18th = new Date(Date.UTC(2020, 5, 18)).getTime()


describe('Given that current date is June 16th, 2020', () => {
    beforeEach(() => {
        cy.clock(june16th)
        cy.visit("https://damian-events.coursedog.com/")
    })
    it('When I click on Today’s Events', () => {
        cy.get('a[title="Today’s Events"]').click()
        cy.wait(3000).reload() // this is flaky, change to alias
        cy.get('#main-content').contains('No events today')
    })
});

describe('Given that current date is June 18th, 2020', () => {
    beforeEach(() => {
        cy.clock(june18th)
        cy.visit("https://damian-events.coursedog.com/")
    })
    it('When I click on Today’s Events', () => {
        cy.get('a[title="Today’s Events"]').click()
        cy.wait(3000).reload() // this is flaky, change to alias
        cy.get('#main-content').contains('QA Recruitment Task Start') // this is failing because the page does not render properly -- bug
    })
    it('When I click on Featured Events', () => {
        cy.get('a[title="Featured Events"]').click()
        cy.wait(3000).reload() // this is flaky, change to alias
        cy.get('div[role="group"]').get('div[role="listitem"]').should('have.length', 3)
    })
    it('When I click on the QA Task Submission event card', () => {
        cy.visit("https://damian-events.coursedog.com/featured")
        // cy.get('a[href="Event name: QA Task Submission"]').click({multiple: true}) // as specified but failing due to no upcomng meeting in last event
        cy.get('a[href="/events/kXPw119C2juN8sV5aCoI/bWPFeUPyU8wpbwW8tzJF"]').click()
        cy.wait(3000).reload() // this is flaky, change to alias
        cy.get('svg[data-icon="calendar-plus"]').should('exist') // this asserts the icon not link
        cy.get('svg[data-icon="google"]').should('exist') // this asserts the icon not link
        cy.get('svg[data-icon="map-marker-alt"]').should('exist') // this asserts the icon not link
        cy.get('a[aria-labeledby="type-QA Recruitment"]').should('exist')
        cy.get('a[aria-labeledby="org-1t3ooeb4fdAhuaxunH5D"]').should('exist') // find a better way to assert org
        cy.get('.my-4').should('have.text', '\n      Upcoming meetings from this event\n    ')
    })
    it('When I use the Search Input in the navigation bar and type in "Finale" and confirm', () => {
        cy.get('input[role="search"]').type('Finale{enter}')
        cy.reload()
        cy.get('#search-results').get('div[role="listitem"]').should('have.length', 1)
    })
    it('When I select the "Coursedog Team" organization from the Filter by Organization dropdown', () => {
        cy.get('#orgSelect').select('Coursedog Team')
        cy.wait(1).reload()  // this is flaky, change to alias
        cy.get('#search-results').get('div[role="listitem"]').should('have.length', 5)
    })
});

describe('When I select a specific date from the calendar', () => {
    beforeEach(() => {
        cy.clock(june18th) // date not specified in scenario
        cy.visit("https://damian-events.coursedog.com/")
    })
    it('I can only see events that happen on that day', () => {
        cy.get('.id-2020-06-22').click() // very day specific, see if possible to reproduce for any day
        cy.wait(3000).reload() // this is flaky, change to alias
        cy.get('.pl-4').should('have.text', '\n          Monday, June 22, 2020')
    })
});

describe('When I select Public Event from the select below CREATE AN EVENT', () => {
    beforeEach(() => {
        cy.visit("https://damian-events.coursedog.com/")
        cy.get('#requestEventTypeSelect').select('Public Events')
        cy.wait(2000)
    })
    it('I can see a "Request A New Event: Public Form" header', () => {
        cy.get('span[class="flex items-center"]').should('have.text', '\n        Request A New Event: Public Form\n      ')
    })
    it('I can see the "Submit" button is disabled when a required field is missing', () => {
        cy.contains('Submit').should('be.disabled')
    })
    it('I can fill the form with the following data', () => {
        cy.get('input[placeholder="Type your email address"]').type('pawel@rebes.pl') // this step is missing in spec
        cy.get('input[placeholder="Set Event Name"]').type('Paweł R')
        cy.get('label').contains('Start date').parent().siblings('input').type('2020-08-16')
        cy.get('label').contains('End date').parent().siblings('input').type('2020-08-17')
        cy.get('legend[id="Featured Event"]').siblings('div[role="radiogroup"]').children('label').children('input[aria-label="Answer yes"]').check()
        cy.get('button').contains('Add Meeting').click()
        cy.get('label').contains('Start Date').parent().siblings('input').type('2020-08-16')
        cy.get('label').contains('End Date').parent().siblings('input').type('2020-08-17')
        cy.get('label').contains('Start Time').parent().siblings('input').type('13:00')
        cy.get('label').contains('End Time').parent().siblings('input').type('14:00')
        cy.get('button').contains('Select Room').click()
        cy.get('h2').contains('Select Room').should('exist')
        cy.get('select[name="Rooms Required Features"]').select('microphone')
        cy.get('button').contains('Search for Available Rooms').click()
        cy.get('button[role="listitem"]').should('have.length', 4)
        cy.get('input[placeholder="Enter room name"]').type('online')
        cy.get('button').contains('Search for Available Rooms').click()
        cy.get('button[role="listitem"]').contains('Online Chat').should('have.length', 1)
        cy.get('button[role="listitem"]').contains('Online Chat').click()
        cy.get('h2').contains('Select Room').should('not.to.exist')
        // missing assertion for a room change
        cy.get('button').contains('Submit').click()
    })
})